import 'package:flutter_test/flutter_test.dart';
import 'package:carpark_go/models/carpark-info-vacancy.dart';
import 'package:carpark_go/services/parking-vacanct-data-api-service.dart';

void main() {
  final ParkingVacancyDataApiService svc = ParkingVacancyDataApiService();

  test(
    'carpark GET request test',
    () async {
      expect(await svc.getCarparks(), isInstanceOf<List<CarparkInfoVacancy>>());
    },
  );

  test(
    'carparks info vacancy GET request test',
    () async {
      expect(await svc.getCarparksInfoVacancy(),
          isInstanceOf<List<CarparkInfoVacancy>>());
    },
  );

  test(
    'carpark info vacancy GET request test',
    () async {
      expect(await svc.getCarparkInfoVacancy('10'),
          isInstanceOf<CarparkInfoVacancy>());
    },
  );
}
