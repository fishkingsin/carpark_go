/*
{
  "park_Id": "10",
  "name": "Kai Tak Cruise Terminal Car Park 1",
  "nature": "commercial",
  "carpark_Type": "off-street",
  "address": {
    "buildingName": "Kai Tak Cruise Terminal",
    "streetName": "Shing Fung Road",
    "buildingNo": "33",
    "floor": "1",
    "subDistrict": "Kowloon Bay",
    "dcDistrict": "Kwun Tong District",
    "region": "KLN"
  },
  "displayAddress": "1st floor, Kai Tak Cruise Terminal, 33 Shing Fung Road, Kowloon Bay, KLN",
  "district": "Kwun Tong District",
  "latitude": 22.3062049,
  "longitude": 114.21309471,
  "contactNo": "+852 3465 6888, 09:30-18:00 Mon-Fri, except public holiday",
  "renditionUrls": {
    "square": "https://sps-opendata.pilotsmartke.gov.hk/rest/getRendition/fs-1%3A693265207413252869411532657339312395903827562313.JPG/square.png",
    "thumbnail": "https://sps-opendata.pilotsmartke.gov.hk/rest/getRendition/fs-1%3A693265207413252869411532657339312395903827562313.JPG/thumbnail.png",
    "banner": "https://sps-opendata.pilotsmartke.gov.hk/rest/getRendition/fs-1%3A693265207413252869411532657339312395903827562313.JPG/banner.png"
  },
  "website": "http://www.kaitakcruiseterminal.com.hk/",
  "opening_status": "OPEN",
  "openingHours": [
    {
      "weekdays": [
        "MON",
        "TUE",
        "WED",
        "THU",
        "FRI",
        "SAT",
        "SUN",
        "PH"
      ],
      "excludePublicHoliday": false,
      "periodStart": "07:00",
      "periodEnd": "23:00"
    }
  ],
  "gracePeriods": [
    {
      "minutes": 10
    }
  ],
  "heightLimits": [
    {
      "height": 2
    }
  ],
  "facilities": [
    "disabilities",
    "evCharger"
  ],
  "paymentMethods": [
    "octopus",
    "visa"
  ],
  "privateCar": {
    "hourlyCharge": [
      {
        "periodStart": "07:00",
        "periodEnd": "23:00",
        "price": 10,
        "type": "hourly",
        "covered": "covered",
        "usageMinimum": 1,
        "remark": "OVERNIGHT PARKING IS NOT AVAILABLE. Parking beyond the opening hours of the carpark will be subject to an administration fee of HK$250 per night on top of the hourly parking rate.",
        "excludePublicHoliday": true,
        "weekdays": [
          "MON",
          "TUE",
          "WED",
          "THU",
          "FRI"
        ]
      },
      {
        "periodStart": "07:00",
        "periodEnd": "23:00",
        "price": 15,
        "usageMinimum": 1,
        "type": "hourly",
        "covered": "covered",
        "remark": "OVERNIGHT PARKING IS NOT AVAILABLE. Parking beyond the opening hours of the carpark will be subject to an administration fee of HK$250 per night on top of the hourly parking rate.",
        "excludePublicHoliday": false,
        "weekdays": [
          "SAT",
          "SUN",
          "PH"
        ]
      }
    ],
    "spaceUNL": 0,
    "spaceEV": 0,
    "spaceDIS": 0,
    "space": 114
  },
  "LGV": {
    "spaceUNL": 0,
    "spaceEV": 0,
    "spaceDIS": 0,
    "space": 0
  },
  "HGV": {
    "spaceUNL": 0,
    "spaceEV": 0,
    "spaceDIS": 0,
    "space": 0
  },
  "coach": {
    "spaceUNL": 0,
    "spaceEV": 0,
    "spaceDIS": 0,
    "space": 0
  },
  "motorCycle": {
    "spaceUNL": 0,
    "spaceEV": 0,
    "spaceDIS": 0,
    "space": 0
  },
  "creationDate": "2016-08-16 10:03:56",
  "modifiedDate": "2018-12-12 12:22:47",
  "publishedDate": "2018-12-12 12:22:47",
  "lang": "en_US"
}
    */
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:math';

class CarparkInfoVacancy {
  CarparkAddress address;
  String parkId;
  String name;
  String nature;
  String carparkType;
  String displayAddress;
  String district;
  double latitude;
  double longitude;
  String contactNo;
  RenditionUrls renditionUrls;
  List<OpeningHour> openingHours;
  List<dynamic> gracePeriods;
  List<dynamic> heightLimits;
  List<dynamic> facilities;
  List<dynamic> paymentMethods;
  PrivateCar privateCar;
  String website;
  String openingStatus;
  String creationDate;
  String modifiedDate;
  String publishedDate;
  String lang;
  LightGoodsVehicle lightGoodsVehicle;
  double distance;
  CarparkInfoVacancy({
    CarparkAddress address,
    String parkId,
    String name,
    String nature,
    String carparkType,
    String displayAddress,
    String district,
    double latitude,
    double longitude,
    String contactNo,
    RenditionUrls renditionUrls,
    List<OpeningHour> openingHours,
    List<dynamic> gracePeriods,
    List<dynamic> heightLimits,
    List<dynamic> facilities,
    List<dynamic> paymentMethods,
    String website,
    String openingStatus,
    String creationDate,
    String modifiedDate,
    String publishedDate,
    String lang,
  });
  CarparkInfoVacancy.fromMap(Map<String, dynamic> map) {
    try {
      parkId = map['park_Id'];
      name = map['name'];
      nature = map['nature'];
      carparkType = map['carpark_Type'];
      address = CarparkAddress.fromMap(map['address']);
      displayAddress = map['displayAddress'];
      district = map['district'];
      latitude = map['latitude'];
      longitude = map['longitude'];
      contactNo = map['contactNo'];
      renditionUrls = RenditionUrls.fromMap(map['renditionUrls']);
      website = map['website'];
      openingStatus = map['opening_status'];
      creationDate = map['creationDate'];
      modifiedDate = map['modifiedDate'];
      publishedDate = map['publishedDate'];
      lang = map['lang'];
      if (map['openingHours'] != null) {
        List<dynamic> openingHoursDyn = map['openingHours'];
        openingHours =
            openingHoursDyn.map((o) => OpeningHour.fromMap(o)).toList();
      }
      gracePeriods = map['gracePeriods'];
      heightLimits = map['heightLimits'];
      facilities = map['facilities'];
      paymentMethods = map['paymentMethods'];
      if (map['privateCar'] != null)
        privateCar = PrivateCar.fromMap(map['privateCar']);
      if (map['LGV'] != null)
        lightGoodsVehicle = LightGoodsVehicle.fromMap(map['LGV']);
      distance = 0;
    } catch (e) {
      print(e.toString());
    }
  }

  calculateDistance(LatLng latLng, String unit) {
    var radlat1 = pi * latLng.latitude / 180;
    var radlat2 = pi * latLng.longitude / 180;
    var radlon1 = pi * this.latitude / 180;
    var radlon2 = pi * this.longitude / 180;
    var theta = latLng.longitude - this.longitude;
    var radtheta = pi * theta / 180;
    var dist = sin(radlat1) * sin(radlat2) +
        cos(radlat1) * cos(radlat2) * cos(radtheta);
    dist = acos(dist);
    dist = dist * 180 / pi;
    dist = dist * 60 * 1.1515;
    if (unit == "K") {
      dist = dist * 1.609344;
    }
    if (unit == "N") {
      dist = dist * 0.8684;
    }
    this.distance = dist;
  }
}

class CarparkAddress {
  String buildingName;
  String streetName;
  String buildingNo;
  String floor;
  String subDistrict;
  String dcDistrict;
  String region;
  CarparkAddress({
    String buildingName,
    String streetName,
    String buildingNo,
    String floor,
    String subDistrict,
    String dcDistrict,
    String region,
  });
  CarparkAddress.fromMap(Map<String, dynamic> map) {
    if (map != null) {
      buildingName = map['buildingName'];
      streetName = map['streetName'];
      buildingNo = map['buildingNo'];
      floor = map['floor'];
      subDistrict = map['subDistrict'];
      dcDistrict = map['dcDistrict'];
      region = map['region'];
    }
  }
}

class RenditionUrls {
  String square;
  String thumbnail;
  String banner;
  RenditionUrls({
    square,
    thumbnail,
    banner,
  });

  RenditionUrls.fromMap(Map<String, dynamic> map) {
    if (map != null) {
      square = map['square'];
      thumbnail = map['thumbnail'];
      banner = map['banner'];
    } else {
      square = '';
      thumbnail = '';
      banner = '';
    }
  }
}

abstract class HasWeekday {
  int getWeekday(w);
  List<int> getWeekdays(List<dynamic> map);
}

class Weekday implements HasWeekday {
  List<int> weekdays;
  fromMap(Map map) {
    List<dynamic> weekdaysDyn = map['weekdays'];
    weekdays = getWeekdays(weekdaysDyn);
  }

  int getWeekday(w) {
    switch (w) {
      case 'MON':
        return DateTime.monday;
      case 'TUE':
        return DateTime.monday;
      case 'WED':
        return DateTime.monday;
      case 'THU':
        return DateTime.monday;
      case 'FRI':
        return DateTime.monday;
      case 'SAT':
        return DateTime.monday;
      case 'SUN':
        return DateTime.monday;
      case 'PH':
        return 0;
      default:
        return -1;
    }
  }

  List<int> getWeekdays(List<dynamic> map) {
    return map.map((w) => getWeekday(w)).toList();
  }
}

class OpeningHour extends Weekday {
  bool excludePublicHoliday;
  String periodStart;
  String periodEnd;
  OpeningHour({weekdays});
  OpeningHour.fromMap(Map<String, dynamic> map) {
    if (map != null) {
      try {
        super.fromMap(map);
      } catch (e) {
        print(e.toString());
      }
      excludePublicHoliday = map['excludePublicHoliday'];
      // var a = map['periodStart'].split(':');
      // periodStart = int.tryParse(a[0]) * 60 * 60 + int.tryParse(a[1]) * 60;
      // var b = map['periodEnd'].split(':');
      // periodEnd = int.tryParse(b[0]) * 60 * 60 + int.tryParse(b[1]) * 60;
      periodStart = map['periodStart'];
      periodEnd = map['periodEnd'];
    } else {}
  }
}

class Charge {
  double price;
  String remark;
  String type;
  String covered;
  String reserved;
  fromMap(Map<String, dynamic> map) {
    price = map['price'];
    remark = map['remark'];
    type = map['type'];
    covered = map['covered'];
    reserved = map['reserved'];
  }
}

class MonthlyCharge extends Charge {}

class HourlyCharge extends Charge with Weekday {
  String periodStart;
  String periodEnd;

  int usageMinimum;
  String remark;
  bool excludePublicHoliday;

  HourlyCharge({
    String periodStart,
    String periodEnd,
    int price,
    String type,
    int covered,
    int usageMinimum,
    String remark,
    bool excludePublicHoliday,
    List<int> weekdays,
  });

  HourlyCharge.fromMap(Map<String, dynamic> map) {
    try {
      super.fromMap(map);
    } catch (e) {
      print(e.toString());
    }
  }
}

class Vehicle {
  int spaceUNL;
  int spaceEV;
  int spaceDIS;
  int space;
  HourlyCharge hourlyCharge;
  MonthlyCharge monthlyCharge;
  Vehicle({
    int spaceUNL,
    int spaceEV,
    int spaceDIS,
    int space,
  });
  fromMap(Map<String, dynamic> map) {
    spaceUNL = map['spaceUNL'];
    spaceEV = map['spaceEV'];
    spaceDIS = map['spaceDIS'];
    space = map['space'];
  }
}

class PrivateCar extends Vehicle {
  PrivateCar({
    int spaceUNL,
    int spaceEV,
    int spaceDIS,
    int space,
  });
  PrivateCar.fromMap(Map<String, dynamic> map) {
    print('create PrivateCar from $map');
    fromMap(map);
  }
}

class LightGoodsVehicle extends Vehicle {
  LightGoodsVehicle({
    int spaceUNL,
    int spaceEV,
    int spaceDIS,
    int space,
  });
  LightGoodsVehicle.fromMap(Map<String, dynamic> map) {
    print('create LightGoodsVehicle from $map');
    fromMap(map);
  }
}
