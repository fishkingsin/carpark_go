import 'package:location/location.dart';
import 'carpark-info-vacancy.dart';

class CarparkCurrentLocation {
  List<CarparkInfoVacancy> _carparks;
  LocationData _currentLocation;

  CarparkCurrentLocation(
      List<CarparkInfoVacancy> carparks, LocationData currentLocation);

  List<CarparkInfoVacancy> get carparks => _carparks;
  set carparks(List<CarparkInfoVacancy> value) {
    _carparks = value;
  }

  LocationData get currentLocation => _currentLocation;
  set currentLocation(LocationData value) {
    _currentLocation = value;
  }
}
