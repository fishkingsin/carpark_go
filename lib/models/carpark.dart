// class Carpark {
//   String park_Id;
//   String name_en;
//   String name_tc;
//   String name_sc;
//   String displayAddress_en;
//   String displayAddress_tc;
//   String displayAddress_sc;
//   double latitude;
//   double longitude;
//   String district_en;
//   String district_tc;
//   String district_sc;
//   String contactNo;
//   String opening_status;
//   double height;
//   String remark_en;
//   String remark_tc;
//   String remark_sc;
//   String website_en;
//   String website_tc;
//   String website_sc;
//   String carpark_photo;

//   Carpark({
//     String park_Id,
//     String name_en,
//     String name_tc,
//     String name_sc,
//     String displayAddress_en,
//     String displayAddress_tc,
//     String displayAddress_sc,
//     double latitude,
//     double longitude,
//     String district_en,
//     String district_tc,
//     String district_sc,
//     String contactNo,
//     String opening_status,
//     double height,
//     String remark_en,
//     String remark_tc,
//     String remark_sc,
//     String website_en,
//     String website_tc,
//     String website_sc,
//     String carpark_photo,
//   });

//   Carpark.fromMap(Map<String, dynamic> map) {
//     park_Id = map['park_id'];
//     name_en = map['name_en'];
//     name_tc = map['name_tc'];
//     name_sc = map['name_sc'];
//     displayAddress_en = map['displayAddress_en'];
//     displayAddress_tc = map['displayAddress_tc'];
//     displayAddress_sc = map['displayAddress_sc'];
//     latitude = map['latitude'];
//     longitude = map['longitude'];
//     district_en = map['district_en'];
//     district_tc = map['district_tc'];
//     district_sc = map['district_sc'];
//     contactNo = map['contactNo'];
//     opening_status = map['opening_status'];
//     height = map['height'].toDouble();
//     remark_en = map['remark_en'];
//     remark_tc = map['remark_tc'];
//     remark_sc = map['remark_sc'];
//     website_en = map['website_en'];
//     website_tc = map['website_tc'];
//     website_sc = map['website_sc'];
//     carpark_photo = map['carpark_photo'];
//   }
// }
