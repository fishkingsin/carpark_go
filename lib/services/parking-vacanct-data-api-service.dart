import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:carpark_go/interfaces/i-parking-vacancy-data-api.dart';
import 'package:carpark_go/models/carpark-info-vacancy.dart';

class ParkingVacancyDataApiService implements IParkingVacancyDataApi {
  final _baseUrl = 'http://api.data.gov.hk';
  final _apiUrl = 'https://api.data.gov.hk';
  final _version = 'v1';
  final language = 'td';
  http.Client _client = http.Client();

  set client(http.Client value) => _client = value;

  static final ParkingVacancyDataApiService _internal =
      ParkingVacancyDataApiService.internal();
  factory ParkingVacancyDataApiService() => _internal;
  ParkingVacancyDataApiService.internal();

  Future<List<CarparkInfoVacancy>> getCarparks() async {
    var url =
        '$_baseUrl/$_version/carpark-info-vacancy?vehicleTypes=privateCar&lang=en_US';
    print('url $url');
    var response = await _client.get(url);
    if (response.statusCode == 200) {
      var data = json.decode(utf8.decode(response.bodyBytes));
      List<dynamic> carparksData = data['results'];
      List<CarparkInfoVacancy> carparks = carparksData
          .map<CarparkInfoVacancy>((c) => CarparkInfoVacancy.fromMap(c))
          .toList();

      return carparks;
    } else {
      throw Exception('Failed to get data');
    }
  }

  Future<List<CarparkInfoVacancy>> getCarparksInfoVacancy() async {
    var url = '$_apiUrl/$_version/carpark-info-vacancy';
    var response = await _client.get(url);
    if (response.statusCode == 200) {
      var data = json.decode(utf8.decode(response.bodyBytes));
      List<dynamic> carparkInfoVacancyDyn = data['results'];
      List<CarparkInfoVacancy> carparkInfoVacancy = carparkInfoVacancyDyn
          .map((c) => CarparkInfoVacancy.fromMap(c))
          .toList();

      return carparkInfoVacancy;
    } else {
      throw Exception('Failed to get data');
    }
  }

  @override
  Future<CarparkInfoVacancy> getCarparkInfoVacancy(String id) async {
    var url =
        '$_apiUrl/$_version/carpark-info-vacancy?data=info&carparkIds=$id';
    var response = await _client.get(url);
    if (response.statusCode == 200) {
      var data = json.decode(utf8.decode(response.bodyBytes));
      List<dynamic> carparkInfoVacancyDyn = data['results'];
      var map = carparkInfoVacancyDyn.elementAt(0);
      CarparkInfoVacancy carparkInfoVacancy = CarparkInfoVacancy.fromMap(map);
      return carparkInfoVacancy;
    } else {
      throw Exception('Failed to get data');
    }
  }
}
