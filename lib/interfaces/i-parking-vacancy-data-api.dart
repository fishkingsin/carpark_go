import 'dart:async';
import 'package:carpark_go/models/carpark-info-vacancy.dart';

abstract class IParkingVacancyDataApi {
  Future<List<CarparkInfoVacancy>> getCarparks();
  Future<CarparkInfoVacancy> getCarparkInfoVacancy(String id);
  Future<List<CarparkInfoVacancy>> getCarparksInfoVacancy();
}
