// import 'dart:async';

// import 'package:carpark_go/interfaces/i-parking-vacancy-data-api.dart';
// import 'package:carpark_go/models/carpark-info-vacancy.dart';
// import 'package:carpark_go/models/carpark.dart';
// import 'package:carpark_go/models/carpark_current_location.dart';
// import 'package:location/location.dart';
// import 'package:meta/meta.dart';
// // import 'package:permission_handler/permission_handler.dart';
// import 'package:scoped_model/scoped_model.dart';

// class MainPageViewModel extends Model {
//   final IParkingVacancyDataApi apiSvc;
//   final location = new Location();
//   Future<List<Carpark>> _carparks;

//   Future<List<Carpark>> get carparks => _carparks;

//   set carparks(Future<List<Carpark>> value) {
//     _carparks = value;
//     notifyListeners();
//   }

//   Future<LocationData> _currentLocation;

//   Future<LocationData> get currentLocation => _currentLocation;

//   set currentLocation(Future<LocationData> value) {
//     _currentLocation = value;
//     notifyListeners();
//   }

//   Future<CarparkCurrentLocation> _carparkCurrentLocation;

//   Future<CarparkCurrentLocation> get carparkCurrentLocation =>
//       _carparkCurrentLocation;

//   set carparkCurrentLocation(Future<CarparkCurrentLocation> value) {
//     _carparkCurrentLocation = value;
//     notifyListeners();
//   }

//   MainPageViewModel({@required this.apiSvc});

//   Future<bool> setCarparks() async {
//     carparks = apiSvc?.getCarparks();
//     return carparks != null;
//   }

//   Future<bool> setLocation() async {
//     try {
//       PermissionStatus permissionStatus = await location.hasPermission();
//       bool granted = (permissionStatus == PermissionStatus.GRANTED);
//       bool hasService = await location.serviceEnabled();
//       if (granted && hasService) {
//         currentLocation = location.getLocation();
//       } else {
//         currentLocation = null;
//         PermissionStatus permissionStatus = await location.requestPermission();
//         if (permissionStatus == PermissionStatus.GRANTED) {
//           currentLocation = location.getLocation();
//         }
//       }
//     } catch (e) {
//       if (e.code == 'PERMISSION_DENIED') {
//         var error = 'Permission denied';
//       }
//       currentLocation = null;
//     }
//     return currentLocation != null;
//   }

//   Future<List<CarparkInfoVacancy>> _carparksInfoVacancy;

//   Future<List<CarparkInfoVacancy>> get carparksInfoVacancy =>
//       _carparksInfoVacancy;

//   set carparksInfoVacancy(Future<List<CarparkInfoVacancy>> value) {
//     _carparksInfoVacancy = value;
//     notifyListeners();
//   }

//   setCarparksInfoVacancy() {
//     carparksInfoVacancy = apiSvc?.getCarparksInfoVacancy();
//     return carparkInfoVacancy != null;
//   }

//   Future<CarparkInfoVacancy> _carparkInfoVacancy;

//   Future<CarparkInfoVacancy> get carparkInfoVacancy => _carparkInfoVacancy;

//   set carparkInfoVacancy(Future<CarparkInfoVacancy> value) {
//     _carparkInfoVacancy = value;
//     notifyListeners();
//   }

//   setCarparkInfoVacancy(String carparkId) {
//     carparkInfoVacancy = apiSvc?.getCarparkInfoVacancy(carparkId);
//     return carparkInfoVacancy != null;
//   }
// }
