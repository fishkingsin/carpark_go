import 'dart:async';
import 'dart:developer' as developer;

import 'package:carpark_go/home/carpark_list/index.dart';
import 'package:carpark_go/models/carpark-info-vacancy.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CarparkListEvent {
  Stream<CarparkListState> applyAsync(
      {CarparkListState currentState, CarparkListBloc bloc});
}

class UnCarparkListEvent extends CarparkListEvent {
  @override
  Stream<CarparkListState> applyAsync(
      {CarparkListState currentState, CarparkListBloc bloc}) async* {
    yield UnCarparkListState();
  }
}

class LoadCarparkListEvent extends CarparkListEvent {
  @override
  Stream<CarparkListState> applyAsync(
      {CarparkListState currentState, CarparkListBloc bloc}) async* {
    try {
      yield InCarparkListState(bloc.carparks, bloc.latLng);
    } catch (_, stackTrace) {
      // developer.log('$_',
      //     name: 'LoadCarparkListEvent', error: _, stackTrace: stackTrace);
      yield ErrorCarparkListState(_?.toString());
    }
  }
}

class ReloadCarparkListEvent extends CarparkListEvent {
  @override
  Stream<CarparkListState> applyAsync(
      {CarparkListState currentState, CarparkListBloc bloc}) async* {
    try {
      yield InCarparkListState(bloc.carparks, bloc.latLng);
    } catch (_, stackTrace) {
      // developer.log('$_',
      //     name: 'LoadCarparkListEvent', error: _, stackTrace: stackTrace);
      yield ErrorCarparkListState(_?.toString());
    }
  }
}

class NavigateToCarParkDetailsEvent extends CarparkListEvent {
  final CarparkInfoVacancy carpark;
  NavigateToCarParkDetailsEvent(this.carpark);

  Stream<CarparkListState> applyAsync(
      {CarparkListState currentState, CarparkListBloc bloc}) async* {
    try {
      yield NavitateToCarparkDetailsState(carpark);
    } catch (_, stackTrace) {
      // developer.log('$_',
      //     name: 'LoadCarparkListEvent', error: _, stackTrace: stackTrace);
      yield ErrorCarparkListState(_?.toString());
    }
  }
}
