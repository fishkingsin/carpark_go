import 'package:carpark_go/home/carpark_list/carpark_list_bloc.dart';
import 'package:carpark_go/home/carpark_list/index.dart';
import 'package:carpark_go/models/carpark-info-vacancy.dart';
import 'package:carpark_go/views/pages/map_route.dart';
import 'package:carpark_go/views/widgets/carpark_info_vacancy_widget.dart';
import 'package:flutter/material.dart';
import 'package:carpark_go/utils/styles.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:transparent_image/transparent_image.dart';

class CarparkListItemWidget extends StatelessWidget {
  final CarparkInfoVacancy carpark;

  CarparkListItemWidget({@required this.carpark});

  @override
  Widget build(BuildContext context) {
    final CarparkListBloc _carparkListBloc =
        BlocProvider.of<CarparkListBloc>(context);

    var title = Text(
      carpark?.name,
      overflow: TextOverflow.fade,
      maxLines: 5,
      style: TextStyle(
        color: Styles.titleColor,
        fontWeight: FontWeight.bold,
        fontSize: Styles.titleFontSize,
      ),
    );

    var subTitle = Text(
      carpark?.displayAddress,
      overflow: TextOverflow.fade,
      maxLines: 5,
      style: new TextStyle(
        fontSize: 13.0,
        fontFamily: 'Roboto',
        color: new Color(0xFF212121),
        fontWeight: FontWeight.bold,
      ),
    );

    return BlocProvider<CarparkListBloc>(
        create: (context) => _carparkListBloc,
        child: Card(
            child: ListTile(
          leading: (carpark.renditionUrls.square != null &&
                  carpark.renditionUrls.square.isNotEmpty)
              ? FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: carpark.renditionUrls.square,
                  fit: BoxFit.cover,
                  width: 50,
                  height: 50,
                )
              : Icon(FontAwesomeIcons.info),
          contentPadding: const EdgeInsets.all(20.0),
          title: title,
          subtitle: subTitle,
          onTap: () {
            _carparkListBloc.add(NavigateToCarParkDetailsEvent(carpark));
            final blockProvider = BlocProvider.of<CarparkListBloc>(context);
            Navigator.of(context).push(
              MaterialPageRoute<CarparkInfoVacancyWidget>(
                builder: (BuildContext context) {
                  return BlocProvider.value(
                      value: blockProvider,
                      child: CarParkDetailsScreen(blocContext: context));
                },
              ),
            );
          },
        )));
  }
}
