import 'package:equatable/equatable.dart';
import 'package:carpark_go/models/carpark-info-vacancy.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class CarparkListState extends Equatable {
  final List propss;
  CarparkListState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

/// UnInitialized
class UnCarparkListState extends CarparkListState {
  UnCarparkListState();

  @override
  String toString() => 'UnCarparkListState';
}

/// Initialized
class InCarparkListState extends CarparkListState {
  final List<CarparkInfoVacancy> carparks;
  final LatLng latLng;

  InCarparkListState(this.carparks, this.latLng) : super([carparks, latLng]);

  @override
  String toString() => 'InCarparkListState $carparks $latLng';
}

class ErrorCarparkListState extends CarparkListState {
  final String errorMessage;

  ErrorCarparkListState(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ErrorCarparkListState';
}

class NavitateToCarparkDetailsState extends CarparkListState {
  final CarparkInfoVacancy carpark;

  NavitateToCarparkDetailsState(this.carpark) : super([carpark]);

  @override
  String toString() => 'NavitateToCarparkDetailsState $carpark';
}
