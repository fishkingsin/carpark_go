import 'package:flutter/material.dart';
import 'package:carpark_go/home/carpark_list/index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CarparkListPage extends StatefulWidget {
  static const String routeName = '/carparkList';

  @override
  _CarparkListPageState createState() => _CarparkListPageState();
}

class _CarparkListPageState extends State<CarparkListPage> {
  @override
  Widget build(BuildContext context) {
    final blockProvider = BlocProvider.of<CarparkListBloc>(context);

    return BlocProvider<CarparkListBloc>(
        create: (context) => blockProvider,
        child: CarparkListScreen(carparkListBloc: blockProvider));
  }
}
