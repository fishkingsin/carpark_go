import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:carpark_go/home/carpark_list/index.dart';
import 'package:carpark_go/models/carpark-info-vacancy.dart';
import 'package:carpark_go/services/index.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class CarparkListBloc extends Bloc<CarparkListEvent, CarparkListState> {
  List<CarparkInfoVacancy> carparks;
  LatLng latLng;
  final Location location = Location();

  CarparkListBloc(CarparkListState initialState) : super(initialState);

  void registerLocationUpdate() {
    location.hasPermission().then((PermissionStatus status) {
      if (status == PermissionStatus.GRANTED) {
        location.onLocationChanged().listen((LocationData currentLocation) {
          this.add(ReloadCarparkListEvent());
        });
      }
    });
  }

  @override
  CarparkListState get initialState => UnCarparkListState();

  @override
  Stream<CarparkListState> mapEventToState(
    CarparkListEvent event,
  ) async* {
    try {
      if (event is LoadCarparkListEvent) {
        ParkingVacancyDataApiService service = ParkingVacancyDataApiService();
        this.carparks = await service.getCarparks();

        final Location location = new Location();
        PermissionStatus status = await location.hasPermission();

        LocationData currentLocation;
        if (status == PermissionStatus.GRANTED) {
          currentLocation = await location.getLocation();
        }
        this.latLng = currentLocation != null
            ? LatLng(currentLocation.latitude, currentLocation.longitude)
            : LatLng(22.3443835, 114.1044375);
        carparks.forEach((element) {
          element.calculateDistance(this.latLng, "K");
        });
        carparks.sort(sortByDistance);
        yield* event.applyAsync(currentState: state, bloc: this);
      } else if (event is ReloadCarparkListEvent) {
        ParkingVacancyDataApiService service = ParkingVacancyDataApiService();
        this.carparks = await service.getCarparks();
        yield* event.applyAsync(currentState: state, bloc: this);
      } else {
        yield* event.applyAsync(currentState: state, bloc: this);
      }
    } catch (_, stackTrace) {
      // developer.log('$_',
      //     name: 'CarparkListBloc', error: _, stackTrace: stackTrace);
      yield state;
    }
  }

  int sortByDistance(CarparkInfoVacancy lhs, CarparkInfoVacancy rhs) {
    return lhs.distance > rhs.distance ? 0 : 1;
  }
}
