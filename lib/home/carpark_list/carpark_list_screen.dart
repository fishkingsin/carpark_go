import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:carpark_go/home/carpark_list/index.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'carpark_listItem_widget.dart';
import 'package:carpark_go/views/widgets/mini_map_widget.dart';
import 'package:carpark_go/models/carpark-info-vacancy.dart';

class CarparkListScreen extends StatefulWidget {
  const CarparkListScreen({
    Key key,
    @required CarparkListBloc carparkListBloc,
  })  : _carparkListBloc = carparkListBloc,
        super(key: key);

  final CarparkListBloc _carparkListBloc;

  @override
  CarparkListScreenState createState() {
    return CarparkListScreenState();
  }
}

class CarparkListScreenState extends State<CarparkListScreen> {
  CarparkListScreenState();
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _load();
    _refreshCompleter = Completer<void>();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CarparkListBloc, CarparkListState>(
        bloc: widget._carparkListBloc,
        builder: (
          BuildContext context,
          CarparkListState currentState,
        ) {
          if (currentState is UnCarparkListState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (currentState is ErrorCarparkListState) {
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(currentState.errorMessage ?? 'Error'),
                Padding(
                  padding: const EdgeInsets.only(top: 32.0),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.resolveWith<Color>(
                        (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed))
                            return Theme.of(context)
                                .colorScheme
                                .primary
                                .withOpacity(0.5);
                          return Colors.blue; // Use the component's default.
                        },
                      ),
                    ),
                    child: Text('reload'),
                    onPressed: _load,
                  ),
                ),
              ],
            ));
          }
          if (currentState is InCarparkListState) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
            return Center(
              child: _displayList(currentState),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  void _load() {
    widget._carparkListBloc.add(LoadCarparkListEvent());
  }

  Widget _displayList(InCarparkListState state) {
    var carparks = state.carparks;
    return RefreshIndicator(
        onRefresh: _getData,
        child: ListView.builder(
          itemCount: carparks == null ? 1 : carparks.length + 1,
          itemBuilder: (context, index) {
            if (index == 0) {
              // return the header
              return _miniMapHeader(state.carparks, state.latLng);
            }
            return CarparkListItemWidget(carpark: state.carparks[index]);
          },
        ));
  }

  Widget _miniMapHeader(List<CarparkInfoVacancy> carparks, LatLng latLng) {
    return Card(
      child: Container(
        width: 256.0,
        height: 256.0,
        child: Stack(fit: StackFit.expand, children: <Widget>[
          Material(
            child: InkWell(
              child: MiniMapWidget(
                carparks: carparks,
                latLng: latLng,
                myLocationEnabled: true,
              ),
            ),
          ),
          Container(
            width: 256.0,
            height: 256.0,
            child: GestureDetector(
              onTap: () {},
            ),
          ),
        ]),
      ),
    );
  }

  Future<void> _getData() {
    _load();
    return _refreshCompleter.future;
  }
}
