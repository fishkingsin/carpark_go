import 'package:carpark_go/home/carpark_list/carpark_list_bloc.dart';
import 'package:carpark_go/home/carpark_list/carpark_list_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:carpark_go/home/index.dart';

import 'carpark_list/index.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key key,
    @required HomeBloc homeBloc,
  })  : _homeBloc = homeBloc,
        super(key: key);

  final HomeBloc _homeBloc;

  @override
  HomeScreenState createState() {
    return HomeScreenState(CarparkListBloc(UnCarparkListState()));
  }
}

class HomeScreenState extends State<HomeScreen> {
  final CarparkListBloc carParkBlock;

  HomeScreenState(this.carParkBlock);

  @override
  void initState() {
    super.initState();
    _load();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
        bloc: widget._homeBloc,
        builder: (
          BuildContext context,
          HomeState currentState,
        ) {
          return BlocProvider<CarparkListBloc>(
            create: (context) => this.carParkBlock,
            child: CarparkListPage(),
          );
        });
  }

  void _load() {
    carParkBlock.add(LoadCarparkListEvent());
    widget._homeBloc.add(LoadHomeEvent());
  }
}
