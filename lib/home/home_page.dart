import 'package:flutter/material.dart';
import 'package:carpark_go/home/index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'navigator/navigator_bloc.dart';

class HomePage extends StatefulWidget {
  static const String routeName = '/home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _homeBloc = HomeBloc(UnHomeState());
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NavigatorBloc>(
        create: (context) => NavigatorBloc(navigatorKey: _navigatorKey),
        child: Scaffold(
          appBar: AppBar(
            title: Text('Home'),
          ),
          body: HomeScreen(homeBloc: _homeBloc),
        )); // return Scaffold(
    //   appBar: AppBar(
    //     title: Text('Home'),
    //   ),
    //   body: HomeScreen(homeBloc: _homeBloc),
    // );
  }
}
