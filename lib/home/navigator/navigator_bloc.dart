import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:equatable/equatable.dart';

class NavigatorBloc extends Bloc<NavigatorEvent, dynamic> {
  final GlobalKey<NavigatorState> navigatorKey;

  NavigatorBloc({this.navigatorKey}) : super(null);

  @override
  dynamic get initialState => "Initial";

  @override
  Stream<dynamic> mapEventToState(NavigatorEvent event) async* {
    if (event is NavigatorEventPop) {
      navigatorKey.currentState.pop();
    } else if (event is NavigatorEventAdd) {
      navigatorKey.currentState.pushNamed('/add');
    }
    yield "Updated";
  }
}

abstract class NavigatorEvent {}

class NavigatorEventPop extends NavigatorEvent {}

class NavigatorEventAdd extends NavigatorEvent {}
