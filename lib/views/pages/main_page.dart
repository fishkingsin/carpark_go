// import 'dart:async';

// import 'package:carpark_go/view_models/main_page_view_model.dart';
// import 'package:carpark_go/views/pages/carpark_panel.dart';
// import 'package:flutter/material.dart';
// import 'package:scoped_model/scoped_model.dart';

// class MainPage extends StatefulWidget {
//   final MainPageViewModel viewModel;

//   MainPage({Key key, @required this.viewModel}) : super(key: key);

//   @override
//   _MainPageState createState() => _MainPageState();
// }

// class _MainPageState extends State<MainPage>
//     with SingleTickerProviderStateMixin {
// //  TabController tabController;

//   Future loadData() async {
//     await widget.viewModel.setCarparks();
//     await widget.viewModel.setLocation();
//   }

//   @override
//   void initState() {
//     super.initState();
// //    tabController = TabController(vsync: this, length: 3);
//     loadData();
//   }

//   var title = Text(
//     'Carparks',
//     style: TextStyle(
//       fontFamily: 'Distant Galaxy',
//     ),
//   );

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       left: false,
//       top: false,
//       right: false,
//       bottom: false,
//       child: Scaffold(
//         appBar: AppBar(
//           centerTitle: true,
//           title: title,
//         ),
//         body: ScopedModel<MainPageViewModel>(
//           model: widget.viewModel,
//           child: CarparksPanel(),
//         ),
//       ),
//     );
//   }

//   @override
//   void dispose() {
// //    tabController?.dispose();
//     super.dispose();
//   }
// }
