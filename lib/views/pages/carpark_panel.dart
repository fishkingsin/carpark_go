// import 'package:carpark_go/models/carpark.dart';
// import 'package:carpark_go/view_models/main_page_view_model.dart';
// import 'package:carpark_go/views/widgets/carparks_list_item.dart';
// import 'package:carpark_go/views/widgets/map_header.dart';
// import 'package:carpark_go/views/widgets/no_internet_connection.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:scoped_model/scoped_model.dart';

// class CarparksPanel extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ScopedModelDescendant<MainPageViewModel>(
//       builder: (context, child, model) {
//         return FutureBuilder<List<Carpark>>(
//           future: model.carparks,
//           builder: (_, AsyncSnapshot<List<Carpark>> snapshot) {
//             switch (snapshot.connectionState) {
//               case ConnectionState.none:
//               case ConnectionState.active:
//               case ConnectionState.waiting:
//                 return Center(child: CircularProgressIndicator());
//               case ConnectionState.done:
//                 if (snapshot.hasData) {
//                   var carparks = snapshot.data;
//                   return ListView.builder(
//                     itemCount: carparks == null ? 1 : carparks.length + 1,
//                     itemBuilder: (_, int index) {
//                       if (index == 0) {
//                         // return the header
//                         return new MapHeader();
//                       }
//                       return CarparksListItem(viewModel: model, carpark: carparks[index - 1]);
//                     },
//                   );
//                 } else if (snapshot.hasError) {
//                   return Center(
//                       child: NoInternetConnection(
//                       action: () async {
//                         await model.setCarparks();
//                       },
//                     )
//                   );
//                 }
//             }
//           },
//         );
//       },
//     );
//   }
// }
