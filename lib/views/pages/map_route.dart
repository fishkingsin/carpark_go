import 'package:carpark_go/home/carpark_list/index.dart';
import 'package:carpark_go/views/widgets/carpark_info_vacancy_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CarParkDetailsScreen extends StatelessWidget {
  const CarParkDetailsScreen({
    Key key,
    this.blocContext,
  }) : super(key: key);

  final BuildContext blocContext;

  static MaterialPageRoute<void> route(BuildContext context) =>
      MaterialPageRoute(
        builder: (_) => CarParkDetailsScreen(blocContext: context),
      );

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CarparkListBloc>(
      // Use existing BLoC instead of creating a new one
      create: (_) => BlocProvider.of<CarparkListBloc>(context),
      child: _CarParkDetailsScreen(),
    );
  }
}

class _CarParkDetailsScreen extends StatefulWidget {
  const _CarParkDetailsScreen({Key key}) : super(key: key);

  @override
  _CarParkDetailsScreenState createState() => _CarParkDetailsScreenState();
}

class _CarParkDetailsScreenState extends State<_CarParkDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(onWillPop: () async {
      Navigator.pop(context);
      BlocProvider.of<CarparkListBloc>(context).add(LoadCarparkListEvent());
      return true;
    }, child: BlocBuilder<CarparkListBloc, CarparkListState>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Details'),
          ),
          body: CarparkInfoVacancyWidget(),
        );
      },
    ));
  }
}
