// import 'package:carpark_go/models/carpark-info-vacancy.dart';
// import 'package:carpark_go/models/carpark.dart';
// import 'package:carpark_go/views/widgets/mini_map.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:carpark_go/view_models/main_page_view_model.dart';
// import 'package:carpark_go/views/widgets/no_internet_connection.dart';
// import 'package:scoped_model/scoped_model.dart';

// class MapPanel extends StatefulWidget {
//   final MainPageViewModel viewModel;
//   MapPanel({Key key, @required this.viewModel}) : super(key: key);

//   @override
//   _MapPanelState createState() => _MapPanelState();
// }

// class _MapPanelState extends State<MapPanel>
//     with SingleTickerProviderStateMixin {
//   Future loadData() async {
//     await widget.viewModel.setCarparks();
//   }

//   @override
//   void initState() {
//     super.initState();
//     loadData();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       left: false,
//       top: false,
//       right: false,
//       bottom: false,
//       child: Scaffold(
//         appBar: AppBar(
//           centerTitle: true,
//           title: Text('Carparks'),
//         ),
//         body: ScopedModel<MainPageViewModel>(
//           model: widget.viewModel,
//           child: _getMap(),
//         ),
//       ),
//     );
//   }

//   Widget _getMap() {
//     return ScopedModelDescendant<MainPageViewModel>(
//         builder: (context, child, model) {
//       return FutureBuilder<List<Carpark>>(
//           future: model.carparks,
//           builder: (_, AsyncSnapshot<List<Carpark>> snapshot) {
//             switch (snapshot.connectionState) {
//               case ConnectionState.none:
//               case ConnectionState.active:
//               case ConnectionState.waiting:
//                 return Center(child: CircularProgressIndicator());
//               case ConnectionState.done:
//                 if (snapshot.hasData) {
//                   var carparks = snapshot.data;
//                   return MiniMapWidget(
//                     carparks: carparks,
//                     myLocationEnabled: true,
//                     rotateGesturesEnabled: true,
//                     scrollGesturesEnabled: true,
//                     tiltGesturesEnabled: true,
//                     zoomGesturesEnabled: true,
//                   );
//                 } else if (snapshot.hasError) {
//                   return NoInternetConnection(
//                     action: () async {
//                       await model.setCarparks();
//                     },
//                   );
//                 }
//             }
//           });
//     });
//   }
// }
