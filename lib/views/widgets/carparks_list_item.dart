// import 'package:carpark_go/models/carpark.dart';
// import 'package:carpark_go/utils/styles.dart';
// import 'package:carpark_go/view_models/main_page_view_model.dart';
// import 'package:carpark_go/views/pages/map_route.dart';
// import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// // https://resource.data.one.gov.hk/ogcio/carpark/Parking_Vacancy_Data_Specification.pdf
// class CarparksListItem extends StatelessWidget {
//   final Carpark carpark;

//   MainPageViewModel viewModel;

//   CarparksListItem({@required this.viewModel, @required this.carpark});

//   @override
//   Widget build(BuildContext context) {
//     var title = Text(
//       carpark?.name != null ? carpark?.name : "",
//       overflow: TextOverflow.ellipsis,
//       style: TextStyle(
//         color: Styles.titleColor,
//         fontWeight: FontWeight.bold,
//         fontSize: Styles.titleFontSize,
//       ),
//     );

//     var subTitle = Row(
//       children: <Widget>[
//         Container(
//           constraints: BoxConstraints(maxWidth: 250),
//           child: Text(
//             carpark?.address != null ? carpark?.address : "",
//             overflow: TextOverflow.ellipsis,
//             style: new TextStyle(
//               fontSize: 13.0,
//               fontFamily: 'Roboto',
//               color: new Color(0xFF212121),
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ),
//       ],
//     );

//     return Card(
//       child: ListTile(
//         leading: (carpark.carpark_photo != null)
//             ? Image.network(
//                 carpark.carpark_photo != null ? carpark.carpark_photo : "",
//                 fit: BoxFit.cover,
//                 width: 50,
//                 height: 50,
//               )
//             : Icon(FontAwesomeIcons.info),
//         contentPadding: const EdgeInsets.all(20.0),
//         title: title,
//         subtitle: subTitle,
//         onTap: () {
//           Navigator.push(
//             context,
//             MaterialPageRoute(
//                 builder: (context) => MapRoute(
//                       viewModel: this.viewModel,
//                       carparkId: this.carpark.park_Id,
//                     )),
//           );
//         },
//       ),
//     );
//   }
// }
