// import 'package:carpark_go/models/carpark.dart';
// import 'package:carpark_go/view_models/main_page_view_model.dart';
// import 'package:carpark_go/views/pages/map_panel.dart';
// import 'package:carpark_go/views/widgets/mini_map.dart';
// import 'package:flutter/material.dart';
// import 'package:scoped_model/scoped_model.dart';

// class MapHeader extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ScopedModelDescendant<MainPageViewModel>(
//         builder: (context, child, model) {
//       return FutureBuilder<List<Carpark>>(
//           future: model.carparks,
//           builder: (_, AsyncSnapshot<List<Carpark>> snapshot) {
//             switch (snapshot.connectionState) {
//               case ConnectionState.none:
//               case ConnectionState.active:
//               case ConnectionState.waiting:
//                 return Center(child: CircularProgressIndicator());
//               case ConnectionState.done:
//                 if (snapshot.hasData) {
//                   List<Carpark> carparks = snapshot.data;

//                   return Card(
//                     child: Container(
//                       width: 256.0,
//                       height: 256.0,
//                       child: Stack(fit: StackFit.expand, children: <Widget>[
//                         Material(
//                           child: InkWell(
//                             child: MiniMapWidget(
//                               carparks: carparks,
//                               myLocationEnabled: true,
//                             ),
//                           ),
//                         ),
//                         Container(
//                           width: 256.0,
//                           height: 256.0,
//                           child: GestureDetector(
//                             onTap: () {
//                               Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                     builder: (context) => MapPanel(
//                                           viewModel: model,
//                                         )),
//                               );
//                             },
//                           ),
//                         ),
//                       ]),
//                     ),
//                   );
//                 } else if (snapshot.hasError) {
//                   return Card(
//                     child: Container(
//                       width: 256.0,
//                       height: 256.0,
//                       child: Center(
//                         child: Text("Error"),
//                       ),
//                     ),
//                   );
//                 }
//             }
//           });
//     });
//   }
// }
