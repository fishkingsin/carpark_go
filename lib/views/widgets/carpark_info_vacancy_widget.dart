import 'package:carpark_go/home/carpark_list/carpark_list_bloc.dart';
import 'package:carpark_go/home/carpark_list/carpark_list_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CarparkInfoVacancyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final CarparkListBloc _carparkListBloc =
        BlocProvider.of<CarparkListBloc>(context);

    return BlocProvider<CarparkListBloc>(
      create: (context) => _carparkListBloc,
      child: Center(
        child: Text(
          "Test",
          style: TextStyle(fontSize: 24.0),
        ),
      ),
    );
  }
}

// import 'dart:async';

// import 'package:carpark_go/models/carpark-info-vacancy.dart';
// import 'package:carpark_go/view_models/main_page_view_model.dart';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:scoped_model/scoped_model.dart';

// class CarparkInfoVacancyWidget extends StatefulWidget {

//   CarparkInfoVacancyWidget({Key key}) : super(key: key);

//   @override
//   State<CarparkInfoVacancyWidget> createState() => CarparkInfoVacancyWidgetState();
// }

// class CarparkInfoVacancyWidgetState extends State<CarparkInfoVacancyWidget> {
//   Completer<GoogleMapController> _controller = Completer();

//   CarparkInfoVacancyWidgetState();

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       body: getGoogleMap(),
//     );
//   }

//   Future<void> _onMarkerTapped() async {
//     // go marker detail
//   }

//   getGoogleMap() {
//     return ScopedModelDescendant<MainPageViewModel>(
//         builder: (context, child, model) {
//       return FutureBuilder<CarparkInfoVacancy>(
//           future: model.carparkInfoVacancy,
//           builder: (_, AsyncSnapshot<CarparkInfoVacancy> snapshot) {
//             switch (snapshot.connectionState) {
//               case ConnectionState.none:
//               case ConnectionState.active:
//               case ConnectionState.waiting:
//                 return Center(child: CircularProgressIndicator());
//               case ConnectionState.done:
//                 if (snapshot.hasData) {
//                   CarparkInfoVacancy carparkInfoVacancy = snapshot.data;
//                   return CustomScrollView(
//                     slivers: <Widget>[
//                       SliverAppBar(
//                         pinned: true,
//                         expandedHeight: 250.0,
//                         flexibleSpace: FlexibleSpaceBar(
//                           title: Text('Demo'),
//                           background: carparkInfoVacancy.renditionUrls.banner != null ? Image.network(
//                           carparkInfoVacancy.renditionUrls.banner,
//                             fit: BoxFit.cover,
//                           ) : null,
//                         ),
//                       ),
//                       SliverGrid(
//                         gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
//                           maxCrossAxisExtent: 200.0,
//                           mainAxisSpacing: 10.0,
//                           crossAxisSpacing: 10.0,
//                           childAspectRatio: 4.0,
//                         ),
//                         delegate: SliverChildBuilderDelegate(
//                               (BuildContext context, int index) {
//                             return Container(
//                               alignment: Alignment.center,
//                               color: Colors.teal[100 * (index % 9)],
//                               child: Text('grid item $index'),
//                             );
//                           },
//                           childCount: 20,
//                         ),
//                       ),
//                       SliverFixedExtentList(
//                         itemExtent: 50.0,
//                         delegate: SliverChildBuilderDelegate(
//                               (BuildContext context, int index) {
//                             return Container(
//                               alignment: Alignment.center,
//                               color: Colors.lightBlue[100 * (index % 9)],
//                               child: Text('list item $index'),
//                             );
//                           },
//                         ),
//                       ),
//                     ],
//                   );
// //                  return GoogleMap(
// //                    myLocationEnabled: true,
// //                    mapType: MapType.normal,
// //                    markers: Set.from([Marker(
// //                          markerId: MarkerId(carparkInfoVacancy.park_Id),
// //                          position: LatLng(carparkInfoVacancy.latitude, carparkInfoVacancy.longitude),
// //                          infoWindow: InfoWindow(title: carparkInfoVacancy.name),
// //                          onTap: _onMarkerTapped,
// //                        )]),
// //                    initialCameraPosition: CameraPosition(
// //                      target:
// //                          LatLng(carparkInfoVacancy.latitude, carparkInfoVacancy.longitude),
// //                      zoom: 14.4746,
// //                    ),
// //                    onMapCreated: (GoogleMapController controller) {
// //                      _controller.complete(controller);
// //                    },
// //                  );
//                 } else if (snapshot.hasError) {
//                   return Card(
//                     child: Container(
//                       child: Center(
//                         child: Text("Error"),
//                       ),
//                     ),
//                   );
//                 }
//             }
//           });
//     });
//   }
// }
