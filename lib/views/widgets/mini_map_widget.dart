import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:carpark_go/models/carpark-info-vacancy.dart';

class MiniMapWidget extends StatefulWidget {
  final List<CarparkInfoVacancy> carparks;
  final LatLng latLng;

  bool myLocationEnabled = true;
  bool rotateGesturesEnabled = false;
  bool scrollGesturesEnabled = false;
  bool tiltGesturesEnabled = false;
  bool zoomGesturesEnabled = false;

  MiniMapWidget({
    Key key,
    @required this.carparks,
    @required this.latLng,
    this.myLocationEnabled,
    this.rotateGesturesEnabled,
    this.scrollGesturesEnabled,
    this.tiltGesturesEnabled,
    this.zoomGesturesEnabled,
  }) : super(key: key);

  @override
  State<MiniMapWidget> createState() => MiniMapWidgetState(
        carparks: carparks,
        latlng: latLng,
        myLocationEnabled: myLocationEnabled,
        rotateGesturesEnabled: rotateGesturesEnabled,
        scrollGesturesEnabled: scrollGesturesEnabled,
        tiltGesturesEnabled: tiltGesturesEnabled,
        zoomGesturesEnabled: zoomGesturesEnabled,
      );
}

class MiniMapWidgetState extends State<MiniMapWidget> {
  Completer<GoogleMapController> _controller = Completer();
  final List<CarparkInfoVacancy> carparks;
  final LatLng latlng;
  // final Location location = new Location();

  bool myLocationEnabled = true;
  bool rotateGesturesEnabled = false;
  bool scrollGesturesEnabled = false;
  bool tiltGesturesEnabled = false;
  bool zoomGesturesEnabled = false;

  MiniMapWidgetState({
    @required this.carparks,
    @required this.latlng,
    this.myLocationEnabled,
    this.rotateGesturesEnabled,
    this.scrollGesturesEnabled,
    this.tiltGesturesEnabled,
    this.zoomGesturesEnabled,
  });

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var markers = Set.of(this.carparks.map((c) => Marker(
          markerId: MarkerId(c.parkId),
          position: LatLng(c.latitude, c.longitude),
          infoWindow: InfoWindow(
            title: c.name,
            onTap: () {},
          ),
        )));
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: new CameraPosition(
        target: latlng,
        zoom: 14.4746,
      ),
      markers: markers,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
    );
  }

  Future<void> _goToCurrent(LocationData data) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
        target: LatLng(data.latitude, data.longitude), zoom: 14.4746)));
  }

  // void registerLocationUpdate() {
  //   this.widget;
  //   location.hasPermission().then((PermissionStatus status) {
  //     if (status == PermissionStatus.GRANTED) {
  //       location.onLocationChanged().listen((LocationData currentLocation) {
  //         _goToCurrent(currentLocation);
  //       });
  //     }
  //   });
  // }
}
